using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TBEasyWebCam;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEngine.Networking;
using SimpleJSON;
using Assets.Scripts.Entity;
using System.IO;

public class QRDecodeTest : MonoBehaviour
{
    public QRCodeDecodeController e_qrController;

    public Text UiText;
    public RawImage mapImage;

    public GameObject resetBtn;
    public Button StopBtn;

    public GameObject scanLineObj;
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
	bool isTorchOn = false;
#endif
    public Sprite torchOnSprite;
    public Sprite torchOffSprite;
    public Image torchImage;
    public String resultText = "";
    public int idEnigme = 0;
    public List<Enigme> enigmes;
    public QRCode qrcode;

    /// <summary>
    /// when you set the var is true,if the result of the decode is web url,it will open with browser.
    /// </summary>
    public bool isOpenBrowserIfUrl;

    private void Start()
    {

        enigmes = JSONToObject_Array.GetEnigmes(PlayerPrefs.GetString("enigmes"));
        Debug.Log("qrcode jjjjjjjjjjjjjj : " + PlayerPrefs.GetString("qrcodeEnigme"));

        if (this.e_qrController != null)
        {
            this.e_qrController.onQRScanFinished += new QRCodeDecodeController.QRScanFinished(this.qrScanFinished);
        }
    }

    private void Update()
    {
    }

    private void qrScanFinished(string dataText)
    {
        if (isOpenBrowserIfUrl) {
            if (Utility.CheckIsUrlFormat(dataText))
            {
                if (!dataText.Contains("http://") && !dataText.Contains("https://"))
                {
                    dataText = "http://" + dataText;
                }
                Application.OpenURL(dataText);
            }
        }
        this.UiText.text = dataText;
        resultText = dataText;
        if (this.resetBtn != null)
        {
            mapImage.gameObject.SetActive(true);
            StopBtn.gameObject.SetActive(false);
            this.resetBtn.SetActive(true);
        }
        if (this.scanLineObj != null)
        {
            this.scanLineObj.SetActive(false);
        }
        this.e_qrController.StopWork();
        if (PlayerPrefs.HasKey("qrcodeEnigme"))
        {
            Debug.Log("aaaaaaaaaaaaaaaaaaaaaaa : " + PlayerPrefs.GetString("qrcodeEnigme"));
            qrcode = JSONToObject_Array.GetQRCode(PlayerPrefs.GetString("qrcodeEnigme"));
            if (resultText.Equals(qrcode.id.ToString()))
            {

                Enigme e = JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme"));
                Debug.Log("enigme jdid howa : " + e.ToString());
                int index = enigmes.FindIndex(en => en.id == e.id);
                this.UiText.text = "index :" + index;
                if (enigmes.Count > index)
                {
                    idEnigme = enigmes[index + 1].id;
                    Debug.Log(" d5al lel local c bon id enigme = " + idEnigme);
                    PlayerPrefs.SetString("enigme", JSONToObject_Array.enigmes[index + 1].ToString());
                    Debug.Log(PlayerPrefs.GetString("enigme"));
                    StartCoroutine(AddInitScore());
                }
            }

        }

    }

    public void Reset()
    {
        if (this.e_qrController != null)
        {
            this.e_qrController.Reset();
        }
        if (this.UiText != null)
        {
            this.UiText.text = string.Empty;
        }
        if (this.resetBtn != null)
        {
            StopBtn.gameObject.SetActive(true);
            mapImage.gameObject.SetActive(false);
            this.resetBtn.SetActive(false);
        }
        if (this.scanLineObj != null)
        {
            this.scanLineObj.SetActive(true);
        }
    }

    public void Play()
    {
        Reset();
        if (this.e_qrController != null)
        {
            this.e_qrController.StartWork();
        }
    }

    public void Stop()
    {
        if (this.e_qrController != null)
        {
            this.e_qrController.StopWork();
        }

        if (this.resetBtn != null)
        {
            this.resetBtn.SetActive(false);
        }
        if (this.scanLineObj != null)
        {
            this.scanLineObj.SetActive(false);
        }
        this.resetBtn.SetActive(true);

        mapImage.gameObject.SetActive(true);
        print("Loading from the device");
        byte[] byteArray = File.ReadAllBytes(Application.persistentDataPath + JSONToObject_Array.GetQRCode(PlayerPrefs.GetString("qrcodeEnigme")).image_map);
        Texture2D texture = new Texture2D(8, 8);
        texture.LoadImage(byteArray);
        //this.GetComponent<Renderer>().material.mainTexture = texture;
        mapImage.texture = texture;

    }

    public void GotoNextScene(string scenename)
    {
        if (this.e_qrController != null)
        {
            this.e_qrController.StopWork();
        }
        //Application.LoadLevel(scenename);
        SceneManager.LoadScene(scenename);
    }

    /// <summary>
    /// Toggles the torch by click the ui button
    /// note: support the feature by using the EasyWebCam Component 
    /// </summary>
    public void toggleTorch()
    {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
		if (EasyWebCam.isActive) {
			if (isTorchOn) {
				torchImage.sprite = torchOffSprite;
				EasyWebCam.setTorchMode (TBEasyWebCam.Setting.TorchMode.Off);
			} else {
				torchImage.sprite = torchOnSprite;
				EasyWebCam.setTorchMode (TBEasyWebCam.Setting.TorchMode.On);
			}
			isTorchOn = !isTorchOn;
		}
#endif
    }

    IEnumerator GetQRCode()
    {
        var url = Adresse.adresse + "qrcode/byId/" + resultText + "/" + JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme")).id;
        Debug.Log("url : " + url);
        UnityWebRequest qrcode = UnityWebRequest.Get(url);

        yield return qrcode.SendWebRequest();

        Debug.Log("Response QRCode:" + qrcode.downloadHandler.text);

        if (qrcode.isNetworkError || qrcode.isHttpError)
        {
            Debug.Log(qrcode.error);
            //	responseText.text = www.error;
        }
        else
        {
            Debug.Log(qrcode.downloadHandler.text);
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(qrcode.downloadHandler.text);
            if (jsoneNode.Count != 0) {
                Debug.Log("5dem");
                Debug.Log("init : " + PlayerPrefs.GetString("enigme"));
                Enigme e = JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme"));

                int index = enigmes.FindIndex(en => en.id == e.id);
                //int index = enigmes.IndexOf(e);
                Debug.Log(index + "e : " + e.ToString() );

                if (enigmes.Count > index)
                {
                    idEnigme = enigmes[index + 1].id;
                    Debug.Log("id enigme = " + idEnigme);
                    PlayerPrefs.SetString("enigme", JSONToObject_Array.enigmes[index + 1].ToString());
                    Debug.Log(PlayerPrefs.GetString("enigme"));
                    StartCoroutine(AddInitScore());
                }
            }
            else
                Debug.Log("ma5demch");
            //PlayerPrefs.SetString("enigmes", qrcode.downloadHandler.text);
            //alert.GetComponent<UnityEngine.UI.Text>().text = "enigmes : " + JSONToObject_Array.enigmes[0].ToString();


            ////Debug.Log(jsoneNode.Count);
            ////bech ya3mel parse lel'array lkol mta3 les questions
        }

    }

    IEnumerator AddInitScore()
    {
        Debug.Log("d5al lel add");
        Debug.Log("teeeeeeeeeeeeeeeeeeeeeeeam : " + PlayerPrefs.GetString("team"));
        Team team = JSONToObject_Array.GetTeamNew(PlayerPrefs.GetString("team"));
        /*WWWForm scoreData = new WWWForm();
        scoreData.AddField("id_user", team.id);
        scoreData.AddField("id_enigme", enigmes[0].id);
        scoreData.AddField("date", new DateTime().ToString());
        scoreData.AddField("score", 0);*/
        //Debug.Log("enigme jdid : " + PlayerPrefs.GetString("enigme")+ " " + idEnigme);
        var url = Adresse.adresse + "score/add";
        //Debug.Log(url);
        //Debug.Log(PlayerPrefs.GetString("team"));
        Debug.Log("hhhhhhhhhhhhhhhhhhhhhhhhhhhhhhkkkkkkkkkkkkk : " + PlayerPrefs.GetString("score"));

        Score s = JSONToObject_Array.GetScore(PlayerPrefs.GetString("score"));
        Debug.Log(s.ToString());
        var score = new UnityWebRequest(url, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(PlayerPrefs.GetString("score"));
        score.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        score.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        score.SetRequestHeader("Content-Type", "application/json");
        List<Score> scores = JSONToObject_Array.scores;

        //Score sc = new Score(team.id,)
        JSONToObject_Array.scores.Add(s);
        //JSONToObject_Array.scores.Add(s);
        String scoresJson = JSONToObject_Array.ScoreJson();

        //JSONToObject_Array.scoresJSON.Insert(JSONToObject_Array.scoresJSON.Length - 1, s.ToString());
        Debug.Log("scoreJdid : " + scoresJson);
        PlayerPrefs.SetString("scores", scoresJson);
        PlayerPrefs.SetString("score", s.ToString());
        yield return score.SendWebRequest();

        Debug.Log("Response ScoreAdd:" + score.downloadHandler.text);

        if (score.isNetworkError || score.isHttpError)
        {
            Debug.Log(score.error);
            PlayerPrefs.SetInt("scoreUpload", 1);
            PlayerPrefs.Save();
            Application.LoadLevel("QuestionPage");

            //	responseText.text = www.error;
        }
        else
        {
            Debug.Log("scoreJdid" + JSONToObject_Array.scoresJSON);
            PlayerPrefs.SetInt("scoreUpload", 0);
            Application.LoadLevel("QuestionPage");

        }


    }

    /// <summary>
    /// ////////////////////// hedhea win wselt 
    /// </summary>
    /// <returns></returns>



}
