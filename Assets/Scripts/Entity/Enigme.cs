﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Entity
{
    [Serializable]
    public class Enigme
    {
        public int id { get; set; }
        public String description { get; set; }
        public DateTime date { get; set; }
        public String video { get; set; }
        public String reponse { get; set; }
        public int ordre { get; set; }
        public int score { get; set; }
        public float temps { get; set; }


        public Enigme(int id, string description, DateTime date, string video, string reponse, int ordre, int score, float temps)
        {
            this.id = id;
            this.description = description;
            this.date = date;
            this.video = video;
            this.reponse = reponse;
            this.ordre = ordre;
            this.score = score;
            this.temps = temps;
        }

        public override string ToString()
        {
            return "{\"id\":" + id + 
                ",\"description\":\"" + description + 
                "\",\"date\":\"" + date +
                "\",\"video\":\"" + video +
                "\",\"reponse\":\"" + reponse +
                "\",\"ordre\":" + ordre +
                ",\"score\":" + score +
                ",\"Temps\":" + temps + "}";
        }


    }

}
