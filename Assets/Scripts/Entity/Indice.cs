﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Entity
{
    [Serializable]
    public class Indice
    {
        public int id { get; set; }
        public String description { get; set; }
        public float temps { get; set; }
        public int id_enigme { get; set; }
        public int score { get; set; }

        public Indice(int id, string description, float temps, int id_enigme, int score)
        {
            this.id = id;
            this.description = description;
            this.temps = temps;
            this.id_enigme = id_enigme;
            this.score = score;
        }

        public Indice(float temps)
        {
            this.temps = temps;
        }

        public override string ToString()
        {
            return "{\"id\":" + id + 
                ",\"description\":\"" + description +
                "\",\"Temps\":" + temps +
                ",\"id_enigme\":" + id_enigme +
                ",\"score\":" + score +"}" ;
        }


    }

}
