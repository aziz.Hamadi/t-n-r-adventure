﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Entity
{
    public class MiseAJour
    {
        public int id { get; set; }
        public DateTime date { get; set; }

        public MiseAJour(int id, DateTime date)
        {
            this.id = id;
            this.date = date;
        }

        public override string ToString()
        {
            return "{\"id\":" + id +
                ",\"date\":\"" + date +
                "\"}";
        }
    }
}
