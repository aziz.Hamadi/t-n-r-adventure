﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Entity
{
    public class QRCode
    {
        public int id { get; set; }
        public String image { get; set; }
        public DateTime date { get; set; }
        public int id_enigme { get; set; }
        public String image_map { get; set; }

        public QRCode(int id, string image, DateTime date, int id_enigme, string image_map)
        {
            this.id = id;
            this.image = image;
            this.date = date;
            this.id_enigme = id_enigme;
            this.image_map = image_map;
        }

        public override string ToString()
        {
            return "{\"id\":" + id +
                ",\"image\":\"" + image +
                "\",\"date\":\"" + date +
                "\",\"id_enigme\":" + id_enigme +
                ",\"image_map\":\"" + image_map +"\"}";
        }

    }
}
