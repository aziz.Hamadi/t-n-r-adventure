﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Entity
{
    [Serializable]
    public class Score
    {
        public int id_user { get; set; }
        public int id_enigme { get; set; }
        public DateTime date { get; set; }
        public int score { get; set; }

        public Score(int id_user, int id_enigme, DateTime date, int score)
        {
            this.id_user = id_user;
            this.id_enigme = id_enigme;
            this.date = date;
            this.score = score;
        }

        public override string ToString()
        {
            return "{\"id_user\":" + id_user +
                ",\"id_enigme\":" + id_enigme + 
                ",\"date\":\"" + date.ToString("yyyy-MM-dd HH:mm:ss") + 
                "\",\"score\":" + score + "}";
        }

    }
}
