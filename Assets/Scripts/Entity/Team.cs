﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Entity
{

    [Serializable]
    public class Team
    {

        public int id { get; set; }
        public string team_name { get; set; }
        public DateTime date { get; set; }
        public int score { get; set; }
        public string password { get; set; }

        public Team(int id, string team_name, DateTime date, int score, string password)
        {
            this.id = id;
            this.team_name = team_name;
            this.date = date;
            this.score = score;
            this.password = password;
        }

        public override string ToString()
        {
            return "{\"id\":" + id +
                ",\"team name\":\"" + team_name +
                "\",\"date\":\"" + date +
                "\",\"score\":" + score +
                ",\"password\":\"" + password +
                "\"}";

        }


        /*public string team_name
            {
                get { return PlayerPrefs.GetString(team_name); }
                set { PlayerPrefs.SetString(team_name, value); }
            }
	
            public string date
            {
                get { return PlayerPrefs.GetString(date); }
                set { PlayerPrefs.SetString(date, value); }
            }
	
            public string password
            {
                get { return PlayerPrefs.GetString(password); }
                set { PlayerPrefs.SetString(password, value); }
            }
            */
    }
}