﻿using Assets.Scripts.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class GenerateEnigme : MonoBehaviour
    {
        public GameObject initEnigmeObject;
        public GameObject enigmeObject;
        public GameObject separateur;
        public GameObject InitSeparateur;
        public Sprite myFirstImage;
        public Sprite mySecondImage;
        public Sprite EmptyImage;
        public Enigme enigme;
        public List<Enigme> enigmes;

        void Start()
        {


            Double position = initEnigmeObject.transform.position.x;
            Double positionSeparateur = InitSeparateur.transform.position.x;
            Debug.Log(PlayerPrefs.GetString("enigmes"));
            enigme = JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme"));
            enigmes = JSONToObject_Array.GetEnigmes(PlayerPrefs.GetString("enigmes"));
            Score score = JSONToObject_Array.GetScore(PlayerPrefs.GetString("score"));


            //hedha eli ena fih 

            if (score.id_enigme == enigme.id) {
                
            }


            Boolean enigmeCourant = false;
            new WaitUntil(() => enigmes.Count > 0);
            Debug.Log("count : " + enigmes.Count);
            for (int i = 0; i < enigmes.Count; i++)
            {
                if (i + 1 < enigmes.Count)
                {
                    GameObject separateurObject = Instantiate(separateur, new Vector3((float)positionSeparateur, InitSeparateur.transform.position.y, InitSeparateur.transform.position.z), Quaternion.identity, GameObject.FindGameObjectWithTag("Canvas").transform);
                    positionSeparateur += 134.03;
                }
            }

            for (int i = 0; i < enigmes.Count ; i++){
                GameObject enigme = Instantiate(enigmeObject, new Vector3((float)position, initEnigmeObject.transform.position.y, initEnigmeObject.transform.position.z), Quaternion.identity, GameObject.FindGameObjectWithTag("Canvas").transform);
                enigme.name = enigmes[i].id.ToString();
                Text enigmeText = enigme.GetComponentInChildren<Transform>().Find("Text")
                    .gameObject.GetComponent<UnityEngine.UI.Text>();
                enigmeText.text = "" + (i + 1);
                Image image = enigme.GetComponent<Image>();
                position -= -134.47;
                
                Debug.Log("a : " + score.ToString() + " b : " + enigmes[i].id + " m : " + enigmes[i].ToString());
                if(enigmeCourant)
                {
                    Debug.Log("d5al lel vide  " + enigmes[i].id);
                    image.sprite = EmptyImage;
                }
                else
                    enigmeText.color = new Color(46f, 49f, 146f);
                if(enigmes[i].id == score.id_enigme)
                {
                    Score s = new Score(score.id_user, this.enigme.id, DateTime.Now, 0);
                    Debug.Log("ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc" + this.enigme.ToString());
                    score = s;
                    //PlayerPrefs.SetString("score")
                }

                if (enigmes[i].id == score.id_enigme && !enigmeCourant)
                {
                    Debug.Log("d5al " + enigmes[i].id);
                    image.sprite = mySecondImage;
                    enigmeText.color = new Color(0,0,0,1);
                    enigmeCourant = true;
                }
                
            }

        }

        // Update is called once per frame
        void Update()
        {
            

        }

    }
}
