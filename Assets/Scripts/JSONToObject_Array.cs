﻿using Assets.Scripts.Entity;
using SimpleJSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts
{
    public class JSONToObject_Array
    {
        public static List<Indice> indices;
        public static List<Enigme> enigmes ;
        public static List<Score> scores;
        public static List<QRCode> qrcodes;
        public static String scoresJSON = "[";
        public static String qrcodesJSON = "[";

        public static List<Indice> GetIndices(String jsonResponse)
        {
            JSONToObject_Array.indices = new List<Indice>();
            List<Indice> indices = new List<Indice>();
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            foreach (JSONNode jsonObject in jsoneNode)
            {
                Indice i = new Indice(jsonObject["id"].AsInt, jsonObject["description"].Value,
                     jsonObject["Temps"].AsFloat,
                    jsonObject["id_enigme"].AsInt, jsonObject["score"].AsInt);
                indices.Add(i);
                JSONToObject_Array.indices.Add(i);
            }
            return indices;
        }

        public static List<Enigme> GetEnigmes(String jsonResponse)
        {
            JSONToObject_Array.enigmes = new List<Enigme>();
            List<Enigme> enigmes = new List<Enigme>();
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            foreach (JSONNode jsonObject in jsoneNode)
            {
                Enigme e = new Enigme(jsonObject["id"].AsInt, jsonObject["description"].Value,
                    Convert.ToDateTime(jsonObject["date"].Value), jsonObject["video"].Value,
                    jsonObject["reponse"].Value, jsonObject["ordre"].AsInt, jsonObject["score"].AsInt, jsonObject["Temps"].AsFloat);
                enigmes.Add(e);
                JSONToObject_Array.enigmes.Add(e);
            }
            return enigmes;
        }

        public static Enigme getEnigme(String jsonResponse)
        {
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            Enigme e = new Enigme(jsoneNode["id"].AsInt, jsoneNode["description"].Value,
                Convert.ToDateTime(jsoneNode["date"].Value), jsoneNode["video"].Value,
                jsoneNode["reponse"].Value, jsoneNode["ordre"].AsInt, jsoneNode["score"].AsInt, jsoneNode["Temps"].AsFloat);
            return e;
        }

        public static Indice getIndice(String jsonResponse)
        {
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            Indice i = new Indice(jsoneNode["id"].AsInt, jsoneNode["description"].Value,
                     jsoneNode["Temps"].AsFloat,
                    jsoneNode["id_enigme"].AsInt, jsoneNode["score"].AsInt);
            return i;
        }


        public static Team GetTeam(String jsonResponse)
        {
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            Team team = new Team(jsoneNode["user"]["id"].AsInt, jsoneNode["user"]["team_name"].Value,
                Convert.ToDateTime(jsoneNode["user"]["date"].Value), jsoneNode["user"]["score"].AsInt,
                jsoneNode["user"]["password"].Value);
            return team;
        }

        public static Team GetTeamNew(String jsonResponse)
        {
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            Team team = new Team(jsoneNode["id"].AsInt, jsoneNode["team_name"].Value,
                Convert.ToDateTime(jsoneNode["date"].Value), jsoneNode["score"].AsInt,
                jsoneNode["password"].Value);
            return team;
        }


        public static List<Score> GetScores(String jsonResponse)
        {
            JSONToObject_Array.scoresJSON = "[";
            List<Score> scores = new List<Score>();
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            for (int i = 0; i < jsoneNode["scores"].Count; i++)
            {
                Score s = new Score(jsoneNode["scores"][i]["id_user"].AsInt, jsoneNode["scores"][i]["id_enigme"].AsInt,
                    Convert.ToDateTime(jsoneNode["scores"][i]["date"].Value), jsoneNode["scores"][i]["score"].AsInt);
                JSONToObject_Array.scoresJSON += s.ToString(); 
                if(i+1 < jsoneNode["scores"].Count)
                    JSONToObject_Array.scoresJSON += ",";
                scores.Add(s);
            }
            JSONToObject_Array.scoresJSON += "]";
            JSONToObject_Array.scores = scores ;
            return scores;
        }

        public static List<Score> GetScoresNew(String jsonResponse)
        {
            JSONToObject_Array.scoresJSON = "[";
            List<Score> scores = new List<Score>();
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            for (int i = 0; i < jsoneNode.Count; i++)
            {
                Score s = new Score(jsoneNode[i]["id_user"].AsInt, jsoneNode[i]["id_enigme"].AsInt,
                    Convert.ToDateTime(jsoneNode[i]["date"].Value), jsoneNode[i]["score"].AsInt);
                JSONToObject_Array.scoresJSON += s.ToString();
                if (i + 1 < jsoneNode.Count)
                    JSONToObject_Array.scoresJSON += ",";
                scores.Add(s);
            }
            JSONToObject_Array.scoresJSON += "]";
            JSONToObject_Array.scores = scores;
            return scores;
        }

        public static Score GetScore(String jsonResponse)
        {
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            Score score = new Score(jsoneNode["id_user"].AsInt, jsoneNode["id_enigme"].AsInt,
                    Convert.ToDateTime(jsoneNode["date"].Value), jsoneNode["score"].AsInt);
            return score;
        }

        public static List<QRCode> GetQRCodes(String jsonResponse)
        {
            JSONToObject_Array.qrcodes = new List<QRCode>();
            List<QRCode> qrcodes = new List<QRCode>();
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            
            for (int i = 0; i < jsoneNode.Count; i++)
            {
                QRCode q = new QRCode(jsoneNode[i]["id"].AsInt, jsoneNode[i]["image"].Value,
                    Convert.ToDateTime(jsoneNode[i]["date"].Value), jsoneNode[i]["id_enigme"].AsInt,
                    jsoneNode[i]["image_map"].Value);
                qrcodes.Add(q);
                JSONToObject_Array.qrcodes.Add(q);
                JSONToObject_Array.qrcodesJSON += q.ToString();
                if (i != 0)
                    JSONToObject_Array.qrcodesJSON += ",";

            }
            return qrcodes;
        }

        public static QRCode GetQRCode(String jsonResponse)
        {
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            QRCode qrcode = new QRCode(jsoneNode["id"].AsInt, jsoneNode["image"].Value,
                    Convert.ToDateTime(jsoneNode["date"].Value), jsoneNode["id_enigme"].AsInt, jsoneNode["image_map"].Value);
            return qrcode;
        }

        public static String ScoreJson()
        {
            String scoreJson = "[";
            List<Score> scores = JSONToObject_Array.scores;

            for (int i = 0; i < scores.Count; i++)
            {
                scoreJson += scores[i].ToString();
                if (i + 1 < scores.Count)
                    scoreJson += ",";
            }
            scoreJson += "]";
            return scoreJson;

        }

        public static MiseAJour GetMiseAJour(String jsonResponse)
        {
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(jsonResponse);
            MiseAJour miseajour = new MiseAJour(jsoneNode["id"].AsInt, Convert.ToDateTime(jsoneNode["date"].Value));
            return miseajour;
        }


    }
}
