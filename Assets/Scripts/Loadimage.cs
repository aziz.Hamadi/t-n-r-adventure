﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using Assets.Scripts;

public class Loadimage : MonoBehaviour
{
    /*
    public RawImage imageToDisplay;
    string url = "http://192.168.192.1:3000/Ressources/Exercice/2d684144a6c2a5e23b9dd57acc155e99.jpg";

    void Start()
    {
        StartCoroutine(loadSpriteImageFromUrl(url));
    }

    IEnumerator loadSpriteImageFromUrl(string URL)
    {
        // Check internet connection
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            yield return null;
        }

        var www = new WWW(URL);
        Debug.Log("Download image on progress");
        yield return www;

        if (string.IsNullOrEmpty(www.text))
        {
            Debug.Log("Download failed");
        }
        else
        {
            Debug.Log("Download succes");
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(www.bytes);
            texture.Apply();

            imageToDisplay.texture = texture;
        }
    }
*/
    public RawImage imageToDisplay;


    IEnumerator Start()
    {
      

        if (File.Exists(Application.persistentDataPath + "/" + JSONToObject_Array.GetQRCode(PlayerPrefs.GetString("qrcodeEnigme")).image_map))
        {
            print("Loading from the device");
            byte[] byteArray = File.ReadAllBytes(Application.persistentDataPath +"/"+ JSONToObject_Array.GetQRCode(PlayerPrefs.GetString("qrcodeEnigme")).image_map);
            Texture2D texture = new Texture2D(8, 8);
            texture.LoadImage(byteArray);
            //this.GetComponent<Renderer>().material.mainTexture = texture;
            imageToDisplay.texture = texture;
        }
        else
        {
            print("Downloading from the web");
            WWW www = new WWW(Adresse.adresse + "/Ressources/map/"+ JSONToObject_Array.GetQRCode(PlayerPrefs.GetString("qrcodeEnigme")).image_map);
            yield return www;
            Texture2D texture = www.texture;
            imageToDisplay.texture = texture;
            byte[] bytes = texture.EncodeToJPG();
            File.WriteAllBytes(Application.persistentDataPath + JSONToObject_Array.GetQRCode(PlayerPrefs.GetString("qrcodeEnigme")).image_map, bytes);
        }



    }
}
