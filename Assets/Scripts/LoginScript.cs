﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using SimpleJSON;
using UnityEngine.SceneManagement;
using System;
using Assets.Scripts;
using Assets.Scripts.Entity;

public class LoginScript : MonoBehaviour
{
    public InputField emailText;
	public InputField passwordText;
    public List<Enigme> enigmes;
    public List<Score> scores;
    private Team team;
    public Boolean test,valider = false;
    public MiseAJour miseAjour;

    // public Text responseText;
    //private Team team;

    void Start()
    {
        //PlayerPrefs.DeleteAll();
        
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
                Debug.Log("Error. Check internet connection!");
                if (PlayerPrefs.HasKey("team") && PlayerPrefs.HasKey("scores") && PlayerPrefs.HasKey("enigmes"))
                    Application.LoadLevel("QuestionPage");
            }
            else
            {

            //appel mta3 ws mta3 mise a jour 
                StartCoroutine(GetAllEnigmes());
                new WaitForEndOfFrame();
                if (PlayerPrefs.HasKey("team") && PlayerPrefs.HasKey("scores"))
                {
                    Team team = JSONToObject_Array.GetTeam(PlayerPrefs.GetString("team"));
                    StartCoroutine(PostDataForLogin(team.team_name,team.password));
                }

            }

    }


    IEnumerator GetMiseAJour()
    {

        var url = Adresse.adresse + "miseAjour";

        UnityWebRequest miseajour = UnityWebRequest.Get(url);

        yield return miseajour.SendWebRequest();

        Debug.Log("Response mise a jour:" + miseajour.downloadHandler.text);

        if (miseajour.isNetworkError || miseajour.isHttpError)
        {
            Debug.Log(miseajour.error);
            //	responseText.text = www.error;
        }
        else
        {

            Debug.Log(miseajour.downloadHandler.text);
            miseAjour = JSONToObject_Array.GetMiseAJour(miseajour.downloadHandler.text);
            if (PlayerPrefs.HasKey("miseajour")) { 
                MiseAJour miseAjourPrefs = JSONToObject_Array.GetMiseAJour(PlayerPrefs.GetString("miseajour"));
                Debug.Log("d5al lel mise a jour : "+ miseAjourPrefs.date + " et "+ miseAjour.date );
                if( miseAjourPrefs.date.CompareTo(miseAjour.date) < 0)
                {
                    Debug.Log(" date jdida ajad mel'9dima");
                    PlayerPrefs.SetString("miseajour", miseajour.downloadHandler.text);

                }
                

            }
            else
            {
                PlayerPrefs.SetString("miseajour", miseajour.downloadHandler.text);

            }
            if (PlayerPrefs.HasKey("team") && PlayerPrefs.HasKey("scores") && PlayerPrefs.HasKey("enigmes"))
                Application.LoadLevel("QuestionPage");


            //alert.GetComponent<UnityEngine.UI.Text>().text = "enigmes : " + JSONToObject_Array.enigmes[0].ToString();


            ////Debug.Log(jsoneNode.Count);
            ////bech ya3mel parse lel'array lkol mta3 les questions
        }

    }

    public void login()
    {
        
        StartCoroutine(PostDataForLogin(emailText.text, passwordText.text));
        /* do
         {
             if (test)
             {
                 if (PlayerPrefs.GetString("team") != null)
                 {
                     //new WaitUntil(() => JSONToObject_Array.scores.Count > 0 && JSONToObject_Array.enigmes.Count > 0 );
                     alert.GetComponent<UnityEngine.UI.Text>().text = "enigme : " + JSONToObject_Array.enigmes.Count ;
                     //StartCoroutine(GetAllScore());
                     //new WaitWhile(() => JSONToObject_Array.scores.Count < 0);
                     //alert.GetComponent<UnityEngine.UI.Text>().text = "score : " + JSONToObject_Array.scores.Count;


                     Application.LoadLevel("QuestionPage");
                     valider = true;
                }
            }
        }
        while (!valider);*/


    }


    IEnumerator PostDataForLogin(String email,String password){

        var url = Adresse.adresse  + "login/"+ email + "/"+ password;
 
		UnityWebRequest login = UnityWebRequest.Get(url);
 
		yield return login.SendWebRequest();
 
		Debug.Log ("Response Login:"+login.downloadHandler.text);
 
		if(login.isNetworkError || login.isHttpError) {
			Debug.Log(login.error);
		//	responseText.text = www.error;
		}
		else {

            Debug.Log("Form upload complete!");
            if(login.downloadHandler.text != "login or password incorrect")
            {

                team = JSONToObject_Array.GetTeam(login.downloadHandler.text);
                scores = JSONToObject_Array.GetScores(login.downloadHandler.text);
                PlayerPrefs.SetString("teamLogin", login.downloadHandler.text);

                PlayerPrefs.SetString("team", team.ToString());
                Debug.Log("nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn : " + PlayerPrefs.GetString("team"));
                PlayerPrefs.Save();
                if(scores.Count <= 0)
                {
                    Score score = new Score(team.id, enigmes[0].id, DateTime.Now, 0);
                    PlayerPrefs.SetString("score", score.ToString());
                    scores.Add(score);
                }
                PlayerPrefs.SetString("score", scores[scores.Count - 1].ToString());

                new WaitUntil(() => PlayerPrefs.HasKey("team") && PlayerPrefs.HasKey("score") && PlayerPrefs.HasKey("enigme"));
                Enigme enigmeCourant = JSONToObject_Array.enigmes.Find(e => e.id == scores[scores.Count - 1].id_enigme);
                PlayerPrefs.SetString("scores", JSONToObject_Array.scoresJSON);
                PlayerPrefs.SetString("enigme", enigmeCourant.ToString());
                PlayerPrefs.Save();

                Application.LoadLevel("QuestionPage");
            }
            else
            {
                Debug.Log(login.downloadHandler.text);
            }
		}
 
	}


    IEnumerator GetAllEnigmes()
    {
        enigmes = new List<Enigme>();

        var url = Adresse.adresse + "enigmes";

        UnityWebRequest enigme = UnityWebRequest.Get(url);

        yield return enigme.SendWebRequest();

        Debug.Log("Response EnigmesListe:" + enigme.downloadHandler.text);

        if (enigme.isNetworkError || enigme.isHttpError)
        {
            Debug.Log(enigme.error);
            //	responseText.text = www.error;
        }
        else
        {
            PlayerPrefs.SetString("enigmes", enigme.downloadHandler.text);

            Debug.Log(PlayerPrefs.GetString("enigmes"));
            enigmes = JSONToObject_Array.GetEnigmes(enigme.downloadHandler.text);
            //alert.GetComponent<UnityEngine.UI.Text>().text = "enigmes : " + JSONToObject_Array.enigmes[0].ToString();
            
            
            ////Debug.Log(jsoneNode.Count);
            ////bech ya3mel parse lel'array lkol mta3 les questions
        }

    }

    IEnumerator GetAllScore()
    {
        team = JSONToObject_Array.GetTeam(PlayerPrefs.GetString("team"));


        scores = new List<Score>();

        var url = Adresse.adresse + "score/byIdUser/"+ team.id;

        UnityWebRequest score = UnityWebRequest.Get(url);

        yield return score.SendWebRequest();

        Debug.Log("Response ScoresListe :" + score.downloadHandler.text);

        if (score.isNetworkError || score.isHttpError)
        {
            Debug.Log(score.error);
            //	responseText.text = www.error;
        }
        else
        {

            Debug.Log("score = " +PlayerPrefs.GetString("scores"));
            scores = JSONToObject_Array.GetScores(score.downloadHandler.text);
            if(scores.Count != 0)
                PlayerPrefs.SetString("score",scores[scores.Count-1].ToString());
            else
                StartCoroutine(AddInitScore());

            PlayerPrefs.SetString("scores", score.downloadHandler.text);

            Debug.Log(JSONToObject_Array.enigmes[0].ToString());
            Enigme enigme = JSONToObject_Array.enigmes.Find(e => e.id == JSONToObject_Array.GetScore(scores[scores.Count - 1].ToString()).id_enigme);
            Debug.Log(enigme.ToString());
            PlayerPrefs.SetString("enigme", enigme.ToString());
            PlayerPrefs.Save();

            ////Debug.Log(jsoneNode.Count);
            ////bech ya3mel parse lel'array lkol mta3 les questions
        }

    }

    IEnumerator AddInitScore()
    {
        Team team = JSONToObject_Array.GetTeam(PlayerPrefs.GetString("team"));
        /*WWWForm scoreData = new WWWForm();
        scoreData.AddField("id_user", team.id);
        scoreData.AddField("id_enigme", enigmes[0].id);
        scoreData.AddField("date", new DateTime().ToString());
        scoreData.AddField("score", 0);*/
        var url = Adresse.adresse + "score/add";

        //Debug.Log(PlayerPrefs.GetString("team"));

        Score s = new Score(team.id, enigmes[0].id, DateTime.Now, 0);
        var score = new UnityWebRequest(url, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(s.ToString());
        score.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        score.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        score.SetRequestHeader("Content-Type", "application/json");

        yield return score.SendWebRequest();

        Debug.Log("Response ScoreAdd:" + score.downloadHandler.text);

        if (score.isNetworkError || score.isHttpError)
        {
            Debug.Log(score.error);
            //	responseText.text = www.error;
        }
        else
        {
            StartCoroutine(GetAllScore());
        }

    }


}
