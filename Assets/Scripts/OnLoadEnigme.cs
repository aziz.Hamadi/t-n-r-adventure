﻿using Assets.Scripts;
using Assets.Scripts.Entity;
using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class OnLoadEnigme : MonoBehaviour
{
    public Team team ;
    public List<Enigme> enigmes;
    public GameObject team_nameUI;
    public GameObject enigme_descriptionUI;
    public GameObject team_scoreUI;
    public Enigme enigme;
    public Score score;
    public List<QRCode> qrcodes;
    public String QRCodeString = "";
    public List<Score> scores;
    public List<Indice> indices;

    public Indice indice;
    public GameObject indice1;
    public GameObject indice2;
    public GameObject indice3;
    public GameObject time;
    public GameObject enigme_num;
    public float minuteEnigme;
    public float minuteIndice1;
    public float minuteIndice2;
    public float minuteIndice3;
    public float second = 0f;
    public float secondIndice1 = 0f;
    public float secondIndice2 = 0f;
    public float secondIndice3 = 0f;
    private double total;
    private TimeSpan dt;



    // Start is called before the first frame update
    void Start()
    {

        QRCodeString = "";
        time = GameObject.FindWithTag("time");
        enigme_num = GameObject.FindWithTag("enigme");
        team_nameUI = GameObject.FindWithTag("team_name");
        team_scoreUI = GameObject.FindWithTag("scoreTeam");

        Debug.Log("aaajkjvbhkjbkjbkjbskjqbkhqcbkqdhv kh : " + PlayerPrefs.GetString("team"));

        enigme_descriptionUI = GameObject.FindWithTag("enigme_description");
        indice1 = GameObject.FindWithTag("indice1");
        indice2 = GameObject.FindWithTag("indice2");
        indice3 = GameObject.FindWithTag("indice3");


        if (PlayerPrefs.HasKey("team") && PlayerPrefs.HasKey("score") && PlayerPrefs.HasKey("enigme"))
        {
            Debug.Log(" rep enigmes jdddiiiiiiid : " + PlayerPrefs.GetString("enigmes"));
            //PlayerPrefs.SetString("enigme", enigmes[1].ToString());

            score = JSONToObject_Array.GetScore(PlayerPrefs.GetString("score"));
            team = JSONToObject_Array.GetTeamNew(PlayerPrefs.GetString("team"));
            team_nameUI.GetComponent<UnityEngine.UI.Text>().text = team.team_name;
            team_scoreUI.GetComponent<UnityEngine.UI.Text>().text = team.score + " PT";
            enigme = JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme"));
            enigme_descriptionUI.GetComponent<TextMeshProUGUI>().text = enigme.description;
            enigme_num.GetComponent<UnityEngine.UI.Text>().text = "ENIGME " + enigme.ordre;

            StartCoroutine(GetAllQRCode());

            if (PlayerPrefs.HasKey("TimeClose"))
            {
                dt = DateTime.Now.Subtract(Convert.ToDateTime(PlayerPrefs.GetString("TimeClose")));
                total = (dt.Hours * 60) + dt.Minutes;
                Debug.Log("temps TimeClose : " + (float)total);
                minuteEnigme = enigme.temps - (float)total;
                if (minuteEnigme < 0)
                {
                    minuteEnigme = 0f;
                    secondIndice1 = PlayerPrefs.GetFloat("secI1");
                    secondIndice2 = PlayerPrefs.GetFloat("secI2");
                    secondIndice3 = PlayerPrefs.GetFloat("secI3");
                }
                else
                {
                    minuteEnigme = enigme.temps;
                    secondIndice1 = PlayerPrefs.GetFloat("secI1");
                    secondIndice2 = PlayerPrefs.GetFloat("secI2");
                    secondIndice3 = PlayerPrefs.GetFloat("secI3");
                }
            }
            else
            {
                minuteEnigme = enigme.temps;
            }


            StartCoroutine(LateStart(0.1f));

        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.Log("Error. Check internet connection!");
        }
        else
        {
            if (PlayerPrefs.HasKey("scoreUpload"))
            {
                if(PlayerPrefs.GetInt("scoreUpload") == 1)
                {
                    StartCoroutine(AddAllScore());
                }
            }
        }
        CalculTempsEnigme();
        AfficherIndices();
    }

    IEnumerator LateStart(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        getIndicesTemps();
    }

    IEnumerator AddAllScore()
    {
        //Team team = JSONToObject_Array.GetTeam(PlayerPrefs.GetString("team"));
        /*WWWForm scoreData = new WWWForm();
        scoreData.AddField("id_user", team.id);
        scoreData.AddField("id_enigme", enigmes[0].id);
        scoreData.AddField("date", new DateTime().ToString());
        scoreData.AddField("score", 0);*/
        PlayerPrefs.SetInt("scoreUpload", 0);

        var url = Adresse.adresse + "score/addAll";

        //Debug.Log(PlayerPrefs.GetString("team"));
        //Score s = new Score(team.id, enigmes[0].id, DateTime.Now, 0);
        var score = new UnityWebRequest(url, "POST");

        Debug.Log("onLoadEnigme : " + PlayerPrefs.GetString("scores"));
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(PlayerPrefs.GetString("scores"));
        score.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        score.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        score.SetRequestHeader("Content-Type", "application/json");

        yield return score.SendWebRequest();

        Debug.Log("Response ScoreAdd all:" + score.downloadHandler.text);

        if (score.isNetworkError || score.isHttpError)
        {
            Debug.Log(score.error);
            //	responseText.text = www.error;
        }
        else
        {
            
            Debug.Log("d5al");
        }
        PlayerPrefs.Save();


    }


    IEnumerator GetAllQRCode()
    {

        qrcodes = new List<QRCode>();

        var url = Adresse.adresse + "qrcode/getAll";
        Debug.Log(url);

        UnityWebRequest qrcode = UnityWebRequest.Get(url);

        yield return qrcode.SendWebRequest();

        Debug.Log("Response QRCode:" + qrcode.downloadHandler.text);

        if (qrcode.isNetworkError || qrcode.isHttpError)
        {

            Debug.Log(qrcode.error);
            //	responseText.text = www.error;
        }
        else
        {
            Debug.Log(qrcode.downloadHandler.text);
            JSONNode jsoneNode = SimpleJSON.JSON.Parse(qrcode.downloadHandler.text);
            qrcodes = JSONToObject_Array.GetQRCodes(qrcode.downloadHandler.text);
            PlayerPrefs.SetString("AllQrcodes", qrcode.downloadHandler.text);
            for (int i = 0 ; i < qrcodes.Count; i++)
            {
                print("Downloading from the enigme page");
                WWW www = new WWW(Adresse.adresse + "/Ressources/map/" + qrcodes[i].image_map);
                yield return www;
                Texture2D texture = www.texture;
                byte[] bytes = texture.EncodeToJPG();
                File.WriteAllBytes(Application.persistentDataPath+ "/" + qrcodes[i].image_map, bytes);

                if (qrcodes[i].id_enigme == enigme.id)
                {
                    QRCodeString = qrcodes[i].ToString();
                }
            }
            PlayerPrefs.SetString("qrcodeEnigme", QRCodeString);
            //alert.GetComponent<UnityEngine.UI.Text>().text = "enigmes : " + JSONToObject_Array.enigmes[0].ToString();
            ////Debug.Log(jsoneNode.Count);
            ////bech ya3mel parse lel'array lkol mta3 les questions
        }

    }

    void OnApplicationQuit()
    {
        //Save temps enigme restant

        enigme.temps = minuteEnigme + 1f;


        PlayerPrefs.SetString("enigme", enigme.ToString());
        //save temps indices restants
        int l = 1;
        for (int i = 0; i < indices.Count; i++)
        {
            if (indices[i].id_enigme == enigme.id && l == 1)
            {

                if (minuteIndice1 > 0f)
                {
                    indices[i].temps = minuteIndice1 + 1f;
                }
                else if (minuteIndice1 == 0)
                {
                    indices[i].temps = minuteIndice1;
                }
                Debug.Log("new temps 1 : " + indices[i].ToString());

                l++;
            }
            else if (indices[i].id_enigme == enigme.id && l == 2)
            {

                if (minuteIndice2 > 0f & minuteIndice1 == 0f)
                {
                    indices[i].temps = minuteIndice2 + 1f;
                }
                else if (minuteIndice2 == 0)
                {
                    indices[i].temps = minuteIndice2;
                }
                Debug.Log("new temps 2 : " + indices[i].ToString());

                l++;
            }
            else if (indices[i].id_enigme == enigme.id && l == 3)
            {

                if (minuteIndice3 > 0f & minuteIndice1 == 0f & minuteIndice2 == 0f)
                {
                    indices[i].temps = minuteIndice3 + 1f;
                }
                else if (minuteIndice3 == 0)
                {
                    indices[i].temps = minuteIndice3;
                }

                Debug.Log("new temps 3 : " + indices[i].ToString());

            }
        }
        string indicesString = string.Join(",", indices.Select(indice => indice.ToString()).ToArray());
        Debug.Log("hedhi liste le5ra : " + "[" + indicesString + "]");
        // PlayerPrefs.DeleteKey("indices");
        PlayerPrefs.SetString("indices", "[" + indicesString + "]");
        // PlayerPrefs.SetString("indices",string.Join( ",", indices ));
        //  Debug.Log("List indices mbadla : "+JSONToObject_Array.GetIndices(PlayerPrefs.GetString("indices")));
        Debug.Log(DateTime.Now.ToString("HH:mm dd MMMM, yyyy"));
        string tRecord = Convert.ToString(DateTime.Now);
        Debug.Log(tRecord);
        PlayerPrefs.SetString("TimeClose", tRecord);


    }

    void CalculTempsEnigme()
    {
        //conpteur Temps Restant Enigme
        if (second <= 0)
        {
            second = 59;

            if (minuteEnigme >= 1)
            {
                minuteEnigme--;
            }
            else
            {
                minuteEnigme = 0;
                second = 0;
                time.GetComponent<UnityEngine.UI.Text>().text = minuteEnigme.ToString("f0") + ":0" + " MIN";

            }
        }
        else
        {
            second -= Time.deltaTime;
        }
        //Affichage dans le UIText du temps restant
        if (Mathf.Round(second) <= 9)
        {
            Debug.Log("time : " + minuteEnigme.ToString("f0") + ":0" + second.ToString("f0"));
            time.GetComponent<UnityEngine.UI.Text>().text = minuteEnigme.ToString("f0") + " MIN";
        }
        else
        {
            Debug.Log("time : " + minuteEnigme.ToString("f0") + ":" + second.ToString("f0"));
            time.GetComponent<UnityEngine.UI.Text>().text = minuteEnigme.ToString("f0") + " MIN";
        }
    }

    void getIndicesTemps()
    {
        //ajout des indices dans une liste
        indices = JSONToObject_Array.GetIndices(PlayerPrefs.GetString("indices"));

        //ajout des minutes de l'indice à leurs variable minute
        int j = 1;
        for (int i = 0; i < indices.Count; i++)
        {
            if (indices[i].id_enigme == enigme.id && j == 1)
            {
                if (PlayerPrefs.HasKey("TimeClose"))
                {

                    dt = DateTime.Now.Subtract(Convert.ToDateTime(PlayerPrefs.GetString("TimeClose")));
                    total = (dt.Hours * 60) + dt.Minutes;
                    minuteIndice1 = indices[i].temps - (float)total;
                    total = (dt.Seconds);
                    if (PlayerPrefs.GetFloat("secI1") == 0)
                    {
                        secondIndice1 = 0;
                    }
                    else
                    {
                        secondIndice1 = PlayerPrefs.GetFloat("secI1") - (float)total;
                    }
                    if (minuteIndice1 < 0)
                    {
                        minuteIndice1 = 0f;
                    }
                }
                else
                {
                    minuteIndice1 = indices[i].temps;
                    secondIndice1 = 0f;
                }
                //minuteIndice1 = indices[i].temps;
                Debug.Log("hedha howa l indice temps ==> " + indices[i].temps);
                j++;
            }
            else if (indices[i].id_enigme == enigme.id && j == 2)
            {
                if (PlayerPrefs.HasKey("TimeClose"))
                {
                    dt = DateTime.Now.Subtract(Convert.ToDateTime(PlayerPrefs.GetString("TimeClose")));
                    total = (dt.Hours * 60) + dt.Minutes;
                    //Debug.Log("temps TimeClose : "+(float)total);
                    minuteIndice2 = indices[i].temps - (float)total;
                    total = (dt.Seconds);
                    Debug.Log("en seconde 2 :" + total);
                    if (PlayerPrefs.GetFloat("secI2") == 0)
                    {
                        secondIndice2 = 0;
                    }
                    else
                    {
                        secondIndice2 = PlayerPrefs.GetFloat("secI2") - (float)total;
                    }

                    if (minuteIndice2 < 0)
                    {
                        minuteIndice2 = 0f;
                    }
                }
                else
                {
                    minuteIndice2 = indices[i].temps;
                    secondIndice2 = 0f;
                }
                //minuteIndice2 = indices[i].temps;
                Debug.Log("hedha howa l indice temps 2 ==> " + indices[i].temps);
                j++;
            }
            else if (indices[i].id_enigme == enigme.id && j == 3)
            {
                if (PlayerPrefs.HasKey("TimeClose"))
                {
                    dt = DateTime.Now.Subtract(Convert.ToDateTime(PlayerPrefs.GetString("TimeClose")));
                    total = (dt.Hours * 60) + dt.Minutes;
                    //Debug.Log("temps TimeClose : "+(float)total);
                    minuteIndice3 = indices[i].temps - (float)total;
                    total = (dt.Seconds);
                    Debug.Log("en seconde 3 :" + total);
                    if (PlayerPrefs.GetFloat("secI3") == 0)
                    {
                        secondIndice3 = 0;
                    }
                    else
                    {
                        secondIndice3 = PlayerPrefs.GetFloat("secI3") - (float)total;
                    }
                    if (minuteIndice3 < 0)
                    {
                        minuteIndice3 = 0f;
                    }
                }
                else
                {
                    minuteIndice3 = indices[i].temps;
                    secondIndice3 = 0f;
                }
                //minuteIndice3 = indices[i].temps;
                Debug.Log("hedha howa l indice temps ==> 3 " + indices[i].temps);
                j++;
            }

        }


    }

    void AfficherIndices()
    {
        //Indice de l'enigme courant
        int k = 1;
        for (int i = 0; i < indices.Count; i++)
        {

            if (indices[i].id_enigme == enigme.id && k == 1)
            {
                if (secondIndice1 <= 0)
                {
                    secondIndice1 = 59;

                    if (minuteIndice1 > 0)
                    {
                        minuteIndice1--;
                    }
                    else
                    {
                        minuteIndice1 = 0;
                        secondIndice1 = 0;
                        indice1.GetComponent<UnityEngine.UI.Text>().text = indices[i].description;
                        indices[i].temps = 0;
                        string indicesString = string.Join(",", indices.Select(indice => indice.ToString()).ToArray());
                        PlayerPrefs.SetString("indices", "[" + indicesString + "]");
                        k++;
                    }
                }
                else
                {
                    secondIndice1 -= Time.deltaTime / indices.Count;
                }

            }
            else if (indices[i].id_enigme == enigme.id && k == 2 && minuteIndice1 == 0)
            {
                if (secondIndice2 <= 0)
                {
                    secondIndice2 = 59;

                    if (minuteIndice2 > 0)
                    {
                        minuteIndice2--;
                    }
                    else
                    {
                        minuteIndice2 = 0;
                        secondIndice2 = 0;
                        indice2.GetComponent<UnityEngine.UI.Text>().text = indices[i].description;
                        indices[i].temps = 0;
                        string indicesString = string.Join(",", indices.Select(indice => indice.ToString()).ToArray());
                        PlayerPrefs.SetString("indices", "[" + indicesString + "]");
                        k++;
                    }
                }
                else
                {
                    secondIndice2 -= Time.deltaTime / 2;
                    PlayerPrefs.SetFloat("secI2", secondIndice2);
                }

            }
            else if (indices[i].id_enigme == enigme.id && k == 3 && minuteIndice2 == 0)
            {
                if (secondIndice3 <= 0)
                {
                    secondIndice3 = 59;

                    if (minuteIndice3 > 0)
                    {
                        minuteIndice3--;
                    }
                    else
                    {
                        minuteIndice3 = 0;
                        secondIndice3 = 0;
                        indice3.GetComponent<UnityEngine.UI.Text>().text = indices[i].description;
                        indices[i].temps = 0;
                        string indicesString = string.Join(",", indices.Select(indice => indice.ToString()).ToArray());
                        Debug.Log("indice jdid hedha : " +  indicesString);
                        PlayerPrefs.SetString("indices", "[" + indicesString + "]");

                    }
                }
                else
                {
                    secondIndice3 -= Time.deltaTime;
                    PlayerPrefs.SetFloat("secI3", secondIndice3);
                }

            }

        }

    }


}
