﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Qrcodegenrate : MonoBehaviour
{

    #region varibales
    public bool QqrcodeStareted = false;
    //private
    [SerializeField]
    public Image statbar;
    [SerializeField]
    public Text Bienvenue;
    // [SerializeField]
    //public bool gamestarted = false;
    [SerializeField]
    public Button taptostart;
    [SerializeField]
    public RawImage mapimage;
    [SerializeField]
    public Image rep;
    [SerializeField]
    public Image scanning;
    [SerializeField]
    public Image animscanning;
    [SerializeField]
    public Button taptostop;
    [SerializeField]
    public Button torche;
    [SerializeField]
    public Text resultat;
    [SerializeField]
    public Text bar;




    #endregion


    // Start is called before the first frame update
    void Start()
    {
        animscanning.gameObject.SetActive(false);
        taptostart.gameObject.SetActive(false);
        taptostop.gameObject.SetActive(false);
        torche.gameObject.SetActive(false);
        scanning.gameObject.SetActive(false);
        resultat.gameObject.SetActive(false);

    }

    public void PlayGame()
    {
        DeviceCameraController.isUseEasyWebCam = true;
        rep.gameObject.SetActive(false);
        mapimage.gameObject.SetActive(false);
        animscanning.gameObject.SetActive(true);
        taptostop.gameObject.SetActive(true);
        torche.gameObject.SetActive(true);
        scanning.gameObject.SetActive(true);

        //taptostart.gameObject.SetActive(true);
        statbar.gameObject.SetActive(true);
    

        QqrcodeStareted = true;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
