﻿using Assets.Scripts;
using Assets.Scripts.Entity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Reponse : MonoBehaviour
{
    InputField input;

    public Team team;
    public Enigme enigme;
    public Indice indice;
    public List<Indice> indices;
    public List<Score> scoresList;
    public Score score;
    public Score s;
    private int TotalScore;
    private float secI1;
    private float secI2;
    private float secI3;

    // Start is called before the first frame update
    void Start()
    {
        input = gameObject.GetComponent<InputField>();
        var reponse = new InputField.SubmitEvent();
        reponse.AddListener(SubmitReponse);
        input.onEndEdit = reponse;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SubmitReponse(string reponse)
    {

        if(reponse.Contains(" " +JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme")).reponse + " ") ||
            ( reponse.EndsWith(JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme")).reponse) &&
            (reponse.Length >= 4 && reponse[reponse.Length - 4].Equals(' ')) ) ||
            reponse.Equals(JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme")).reponse)) { 
            Debug.Log( reponse + " vrai");
            
            if (PlayerPrefs.HasKey("team") && PlayerPrefs.HasKey("enigme") /*&& PlayerPrefs.HasKey("secI1") && PlayerPrefs.HasKey("secI2") && PlayerPrefs.HasKey("secI3")*/)
            {
                team = JSONToObject_Array.GetTeamNew(PlayerPrefs.GetString("team"));
                enigme = JSONToObject_Array.getEnigme(PlayerPrefs.GetString("enigme"));
                indices = JSONToObject_Array.GetIndices(PlayerPrefs.GetString("indices"));
                secI1 = PlayerPrefs.GetFloat("secI1");
                secI2 = PlayerPrefs.GetFloat("secI2");
                secI3 = PlayerPrefs.GetFloat("secI3");
                Debug.Log("sec 1 ,"+secI1+"sec 2 ,"+secI2+"sec 3 ,"+secI3);
                TotalScore = enigme.score;
                int j=1;
                for(int i=0 ; i < indices.Count ; i++){
                    if(indices[i].temps == 0 && j==1 && indices[i].id_enigme == enigme.id && secI1 <= 0 )
                    {
                        TotalScore = TotalScore - indices[i].score ;
                        j++;
                    }else if(indices[i].temps == 0 && j==2 && indices[i].id_enigme == enigme.id && secI2 <= 0)
                    {
                        TotalScore = TotalScore - indices[i].score ;
                        j++;
                    }else if(indices[i].temps == 0 && j==3 && indices[i].id_enigme == enigme.id && secI3 <= 0)
                    {
                        TotalScore = TotalScore - indices[i].score ;
                    }
                }
                team.score = team.score + TotalScore ;


                //hedhi l'mochklaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa

                PlayerPrefs.SetString("team",team.ToString());

                Debug.Log("Team Reponse : "+team.ToString());
                score.id_user = team.id;
                score.id_enigme = enigme.id;
                score.date = DateTime.Now;
                score.score = TotalScore;
                s = JSONToObject_Array.GetScore(PlayerPrefs.GetString("score"));
                s.score = TotalScore;
                Debug.Log("Team score : " + score.ToString());

                PlayerPrefs.SetString("score", score.ToString());

                scoresList = JSONToObject_Array.scores;
                Debug.Log("scores count : "+scoresList.Count);
                for (int i = 0; i < scoresList.Count; i++)
                {
                    if(scoresList[i].id_user == team.id && scoresList[i].id_enigme == enigme.id){
                        scoresList.Insert(i, score );
                        scoresList.RemoveAt(i+1);
                        string scoresString = string.Join(",", scoresList.Select(score => score.ToString()).ToArray()) ;
                        PlayerPrefs.SetString("scores","["+scoresString+"]");
                        Debug.Log("score jdod : "+"["+scoresString+"]");
                        PlayerPrefs.DeleteKey("TimeClose");
                        PlayerPrefs.DeleteKey("secI1");
                        PlayerPrefs.DeleteKey("secI2");
                        PlayerPrefs.DeleteKey("secI3");
                    }
                }                
                
            }
            Application.LoadLevel("SuccessPage");
        }
        else
            Debug.Log(reponse + " faux");
    }

}